| Name                                                      | Self CPU % | Self CPU  | CPU total % | CPU total | CPU time avg | Self CUDA | Self CUDA % | CUDA total | CUDA time avg | # of Calls   |
|-----------------------------------------------------------|------------|-----------|-------------|-----------|--------------|-----------|-------------|------------|---------------|--------------|
| cudaLaunchKernel                                          | 13.77%     | 2.395s    | 0.1378      | 2.396s    | 11.717us     | 536.574ms | 8.04%       | 537.316ms  | 2.628us       | 204458       |
| aten::addmm                                               | 4.56%      | 792.411ms | 0.0988      | 1.718s    | 121.837us    | 1.021s    | 15.31%      | 1.134s     | 80.416us      | 14100        |
| aten::copy_                                               | 0.89%      | 155.564ms | 0.0799      | 1.390s    | 32.854us     | 846.297ms | 12.68%      | 920.378ms  | 21.758us      | 42300        |
| Optimizer.step#Adam.step                                  | 13.07%     | 2.272s    | 0.2699      | 4.693s    | 665.650us    | 0.000us   | 0.00%       | 4.016s     | 569.633us     | 7050         |
| volta_sgemm_128x64_tn                                     | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 857.179ms | 12.85%      | 857.179ms  | 121.586us     | 7050         |
| Memcpy   HtoD (Pinned -> Device)                          | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 846.297ms | 12.68%      | 846.297ms  | 60.021us      | 14100        |
| aten::mm                                                  | 3.85%      | 668.920ms | 0.0588      | 1.022s    | 48.340us     | 566.594ms | 8.49%       | 678.271ms  | 32.070us      | 21150        |
| cudaStreamSynchronize                                     | 6.14%      | 1.067s    | 0.0614      | 1.068s    | 75.729us     | 37.088ms  | 0.56%       | 37.457ms   | 2.657us       | 14100        |
| volta_sgemm_128x64_nt                                     | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 404.326ms | 6.06%       | 404.326ms  | 28.676us      | 14100        |
| enumerate(DataLoader)#_MultiProcessingDataLoaderIter...   | 5.56%      | 966.769ms | 0.0563      | 978.755ms | 138.811us    | 0.000us   | 0.00%       | 214.000us  | 0.030us       | 7051         |
| aten::mean                                                | 2.10%      | 365.499ms | 0.0292      | 507.692ms | 35.678us     | 107.933ms | 1.62%       | 147.645ms  | 10.376us      | 14230        |
| aten::empty_strided                                       | 3.67%      | 638.387ms | 0.0368      | 639.330ms | 6.045us      | 0.000us   | 0.00%       | 618.000us  | 0.006us       | 105758       |
| aten::sum                                                 | 2.07%      | 360.091ms | 0.0298      | 518.027ms | 36.740us     | 106.716ms | 1.60%       | 146.315ms  | 10.377us      | 14100        |
| aten::div                                                 | 2.61%      | 454.518ms | 0.0419      | 727.911ms | 51.621us     | 35.268ms  | 0.53%       | 71.942ms   | 5.102us       | 14101        |
| cudaFree                                                  | 3.11%      | 541.122ms | 0.0332      | 577.988ms | 192.663ms    | 23.000us  | 0.00%       | 88.000us   | 29.333us      | 3            |
| volta_sgemm_32x32_sliced1x4_tn                            | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 163.868ms | 2.46%       | 163.868ms  | 23.244us      | 7050         |
|     autograd::engine::evaluate_function:   AddmmBackward0 | 2.44%      | 425.143ms | 0.1409      | 2.450s    | 173.763us    | 0.000us   | 0.00%       | 824.586ms  | 58.481us      | 14100        |
| volta_sgemm_128x64_nn                                     | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 141.005ms | 2.11%       | 141.005ms  | 20.001us      | 7050         |
| Optimizer.zero_grad#Adam.zero_grad                        | 1.91%      | 332.357ms | 0.0192      | 333.268ms | 47.272us     | 0.000us   | 0.00%       | 605.000us  | 0.086us       | 7050         |
| aten::t                                                   | 1.80%      | 313.216ms | 0.0307      | 533.939ms | 8.415us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 63450        |
| cudaOccupancyMaxActiveBlocksPerMultiprocessor             | 0.31%      | 53.303ms  | 0.0031      | 53.303ms  | 1.512us      | 93.353ms  | 1.40%       | 93.353ms   | 2.648us       | 35253        |
| void   at::native::reduce_kernel<512, 1, at::native::R... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 107.933ms | 1.62%       | 107.933ms  | 7.655us       | 14100        |
| void   at::native::reduce_kernel<128, 4, at::native::R... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 106.716ms | 1.60%       | 106.716ms  | 7.569us       | 14100        |
| cudaMemcpyAsync                                           | 0.96%      | 166.777ms | 0.0096      | 166.865ms | 11.834us     | 36.610ms  | 0.55%       | 36.610ms   | 2.596us       | 14100        |
| aten::sub                                                 | 0.93%      | 162.234ms | 0.0145      | 251.642ms | 35.694us     | 21.149ms  | 0.32%       | 39.954ms   | 5.667us       | 7050         |
| aten::threshold_backward                                  | 1.01%      | 175.643ms | 0.0162      | 282.379ms | 40.054us     | 14.184ms  | 0.21%       | 31.940ms   | 4.530us       | 7050         |
| aten::sigmoid                                             | 0.89%      | 155.133ms | 0.0133      | 230.519ms | 32.698us     | 16.153ms  | 0.24%       | 35.573ms   | 5.046us       | 7050         |
| autograd::engine::evaluate_function:   MeanBackward0      | 1.13%      | 196.825ms | 0.0645      | 1.121s    | 79.508us     | 0.000us   | 0.00%       | 71.942ms   | 5.102us       | 14100        |
| AddmmBackward0                                            | 1.10%      | 191.449ms | 0.0844      | 1.467s    | 104.069us    | 0.000us   | 0.00%       | 678.271ms  | 48.104us      | 14100        |
| aten::abs                                                 | 0.84%      | 145.731ms | 0.0267      | 464.897ms | 32.971us     | 14.181ms  | 0.21%       | 65.770ms   | 4.665us       | 14100        |
| aten::_to_copy                                            | 1.03%      | 178.551ms | 0.1         | 1.739s    | 41.106us     | 0.000us   | 0.00%       | 918.478ms  | 21.713us      | 42300        |
| aten::sigmoid_backward                                    | 0.77%      | 134.118ms | 0.0146      | 253.295ms | 35.928us     | 16.925ms  | 0.25%       | 33.580ms   | 4.763us       | 7050         |
| autograd::engine::evaluate_function:   torch::autograd... | 1.00%      | 173.615ms | 0.0212      | 368.792ms | 13.078us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200        |
| cudaPointerGetAttributes                                  | 0.47%      | 82.044ms  | 0.0048      | 83.596ms  | 5.929us      | 34.870ms  | 0.52%       | 35.104ms   | 2.490us       | 14100        |
| aten::mul                                                 | 0.74%      | 128.092ms | 0.0115      | 200.556ms | 28.448us     | 15.992ms  | 0.24%       | 33.428ms   | 4.742us       | 7050         |
| aten::clamp_min                                           | 0.69%      | 119.263ms | 0.0115      | 200.469ms | 28.435us     | 16.913ms  | 0.25%       | 35.778ms   | 5.075us       | 7050         |
| aten::sgn                                                 | 0.73%      | 126.419ms | 0.0125      | 216.598ms | 30.723us     | 14.272ms  | 0.21%       | 33.378ms   | 4.734us       | 7050         |
| aten::to                                                  | 0.83%      | 143.897ms | 0.1033      | 1.796s    | 42.453us     | 0.000us   | 0.00%       | 871.806ms  | 20.608us      | 42304        |
| aten::transpose                                           | 0.79%      | 137.708ms | 0.0126      | 218.645ms | 3.446us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 63450        |
| aten::empty                                               | 0.71%      | 122.713ms | 0.0071      | 122.975ms | 5.812us      | 0.000us   | 0.00%       | 81.000us   | 0.004us       | 21158        |
| aten::as_strided                                          | 0.67%      | 117.244ms | 0.0067      | 117.254ms | 1.279us      | 0.000us   | 0.00%       | 52.000us   | 0.001us       | 91650        |
| cudaMemsetAsync                                           | 0.37%      | 64.486ms  | 0.0037      | 64.551ms  | 9.156us      | 18.364ms  | 0.28%       | 18.387ms   | 2.608us       | 7050         |
| aten::fill_                                               | 0.36%      | 62.961ms  | 0.0088      | 153.845ms | 21.797us     | 14.161ms  | 0.21%       | 31.936ms   | 4.525us       | 7058         |
| MeanBackward0                                             | 0.56%      | 98.009ms  | 0.0531      | 923.515ms | 65.498us     | 0.000us   | 0.00%       | 71.822ms   | 5.094us       | 14100        |
| aten::relu                                                | 0.51%      | 87.862ms  | 0.0166      | 288.550ms | 40.929us     | 0.000us   | 0.00%       | 35.825ms   | 5.082us       | 7050         |
| aten::view                                                | 0.47%      | 81.696ms  | 0.0047      | 81.715ms  | 3.864us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 21150        |
| autograd::engine::evaluate_function:   AbsBackward0       | 0.47%      | 81.051ms  | 0.0321      | 557.752ms | 79.114us     | 0.000us   | 0.00%       | 66.806ms   | 9.476us       | 7050         |
| torch::autograd::AccumulateGrad                           | 0.46%      | 80.758ms  | 0.0112      | 194.227ms | 6.887us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200        |
| autograd::engine::evaluate_function:   TBackward0         | 0.45%      | 79.059ms  | 0.0129      | 223.642ms | 15.861us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100        |
| aten::expand                                              | 0.43%      | 74.069ms  | 0.0057      | 98.324ms  | 6.973us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100        |
| autograd::engine::evaluate_function:   ReluBackward0      | 0.40%      | 69.382ms  | 0.0225      | 391.591ms | 55.545us     | 0.000us   | 0.00%       | 31.940ms   | 4.530us       | 7050         |
| autograd::engine::evaluate_function:   SigmoidBackward... | 0.38%      | 66.197ms  | 0.0208      | 362.032ms | 51.352us     | 0.000us   | 0.00%       | 33.580ms   | 4.763us       | 7050         |
| detach                                                    | 0.38%      | 65.572ms  | 0.0038      | 65.572ms  | 2.325us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200        |
| aten::item                                                | 0.36%      | 63.122ms  | 0.004       | 68.900ms  | 1.222us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 56402        |
| aten::l1_loss                                             | 0.36%      | 63.040ms  | 0.0487      | 847.017ms | 120.144us    | 0.000us   | 0.00%       | 172.776ms  | 24.507us      | 7050         |
| aten::linear                                              | 0.35%      | 60.912ms  | 0.1117      | 1.943s    | 137.788us    | 0.000us   | 0.00%       | 1.134s     | 80.395us      | 14100        |
| cudaStreamIsCapturing                                     | 0.05%      | 8.899ms   | 0.0005      | 8.899ms   | 1.261us      | 19.153ms  | 0.29%       | 19.153ms   | 2.715us       | 7055         |
| AbsBackward0                                              | 0.34%      | 59.547ms  | 0.0274      | 476.701ms | 67.617us     | 0.000us   | 0.00%       | 66.806ms   | 9.476us       | 7050         |
| aten::reshape                                             | 0.33%      | 57.498ms  | 0.0047      | 82.598ms  | 11.716us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 21.149ms  | 0.32%       | 21.149ms   | 3.000us       | 7050         |
| void   at::native::elementwise_kernel<128, 2, at::nati... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 21.150ms  | 0.32%       | 21.150ms   | 3.000us       | 7050         |
| void   splitKreduce_kernel<32, 16, int, float, float, ... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 21.263ms  | 0.32%       | 21.263ms   | 3.016us       | 7050         |
| aten::_reshape_alias                                      | 0.30%      | 51.477ms  | 0.003       | 51.477ms  | 3.651us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100        |
| aten::detach                                              | 0.28%      | 48.847ms  | 0.0065      | 113.130ms | 4.012us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200        |
| cudaHostAlloc                                             | 0.25%      | 42.859ms  | 0.0025      | 42.859ms  | 2.381ms      | 70.000us  | 0.00%       | 70.000us   | 3.889us       | 18           |
| aten::flatten                                             | 0.25%      | 44.262ms  | 0.004       | 69.982ms  | 9.927us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 16.913ms  | 0.25%       | 16.913ms   | 2.399us       | 7050         |
| autograd::engine::evaluate_function:   SubBackward0       | 0.25%      | 43.824ms  | 0.0026      | 45.733ms  | 6.487us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 16.925ms  | 0.25%       | 16.925ms   | 2.401us       | 7050         |
| aten::resize_                                             | 0.24%      | 41.495ms  | 0.0024      | 41.556ms  | 5.891us      | 0.000us   | 0.00%       | 55.000us   | 0.008us       | 7054         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 16.153ms  | 0.24%       | 16.153ms   | 2.291us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 15.992ms  | 0.24%       | 15.992ms   | 2.268us       | 7050         |
| SigmoidBackward0                                          | 0.24%      | 42.540ms  | 0.017       | 295.835ms | 41.962us     | 0.000us   | 0.00%       | 33.580ms   | 4.763us       | 7050         |
| ReluBackward0                                             | 0.23%      | 39.830ms  | 0.0185      | 322.209ms | 45.703us     | 0.000us   | 0.00%       | 31.940ms   | 4.530us       | 7050         |
| autograd::engine::evaluate_function:   ViewBackward0      | 0.22%      | 37.669ms  | 0.0083      | 143.629ms | 20.373us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 14.181ms  | 0.21%       | 14.181ms   | 2.011us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 14.161ms  | 0.21%       | 14.161ms   | 2.006us       | 7058         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 14.118ms  | 0.21%       | 14.118ms   | 2.003us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 14.272ms  | 0.21%       | 14.272ms   | 2.024us       | 7050         |
| void   at::native::vectorized_elementwise_kernel<4, at... | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 14.184ms  | 0.21%       | 14.184ms   | 2.012us       | 7050         |
| aten::ones_like                                           | 0.19%      | 32.335ms  | 0.0163      | 283.674ms | 40.237us     | 0.000us   | 0.00%       | 31.669ms   | 4.492us       | 7050         |
| TBackward0                                                | 0.17%      | 29.160ms  | 0.0081      | 140.065ms | 9.934us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100        |
| ViewBackward0                                             | 0.13%      | 22.741ms  | 0.0059      | 102.149ms | 14.489us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| aten::empty_like                                          | 0.11%      | 18.466ms  | 0.0058      | 100.282ms | 14.208us     | 0.000us   | 0.00%       | 228.000us  | 0.032us       | 7058         |
| aten::randperm                                            | 0.09%      | 15.665ms  | 0.0018      | 31.380ms  | 7.845ms      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 4            |
| aten::_local_scalar_dense                                 | 0.04%      | 6.890ms   | 0.0004      | 6.890ms   | 0.122us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 56402        |
| cudaMalloc                                                | 0.01%      | 1.859ms   | 0.0001      | 1.859ms   | 169.000us    | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 11           |
| Memset   (Device)                                         | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 355.000us | 0.01%       | 355.000us  | 0.050us       | 7050         |
| aten::broadcast_tensors                                   | 0.01%      | 2.381ms   | 0.0001      | 2.381ms   | 0.338us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| SubBackward0                                              | 0.01%      | 1.909ms   | 0.0001      | 1.909ms   | 0.271us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050         |
| aten::random_                                             | 0.00%      | 59.000us  | 0           | 59.000us  | 29.500us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2            |
| aten::scalar_tensor                                       | 0.00%      | 10.000us  | 0           | 10.000us  | 5.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2            |
| aten::resolve_conj                                        | 0.00%      | 1.000us   | 0           | 1.000us   | 0.500us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2            |
| aten::resolve_neg                                         | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2            |
| cudaDeviceGetAttribute                                    | 0.00%      | 2.000us   | 0           | 2.000us   | 0.143us      | 23.000us  | 0.00%       | 23.000us   | 1.643us       | 14           |
| cudaGetSymbolAddress                                      | 0.00%      | 1.000us   | 0           | 1.000us   | 1.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 1            |
| aten::lift_fresh                                          | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 4            |
| aten::detach_                                             | 0.00%      | 5.000us   | 0           | 5.000us   | 1.250us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 4            |
| detach_                                                   | 0.00%      | 0.000us   | 0           | 0.000us   | 0.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 4            |
| aten::zeros_like                                          | 0.00%      | 32.000us  | 0           | 437.000us | 54.625us     | 0.000us   | 0.00%       | 200.000us  | 25.000us      | 8            |