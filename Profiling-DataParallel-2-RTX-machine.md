# Profiling-Computer-Architecture-VIS

| Name                                                     | Self CPU % | Self CPU  | CPU total % | CPU total | CPU time avg | Self CUDA | Self CUDA % | CUDA total | CUDA time avg | \# of Calls |
| -------------------------------------------------------- | ---------- | --------- | ----------- | --------- | ------------ | --------- | ----------- | ---------- | ------------- | ----------- |
| DataParallel.forward                                     | 23.95%     | 13.016s   | 42.77%      | 23.237s   | 3.296ms      | 0.000us   | 0.00%       | 20.223s    | 2.868ms       | 7050        |
| cudaLaunchKernel                                         | 10.01%     | 5.438s    | 10.03%      | 5.449s    | 13.418us     | 2.882s    | 6.78%       | 2.901s     | 7.144us       | 406102      |
| aten::copy_                                              | 4.72%      | 2.562s    | 11.68%      | 6.346s    | 90.014us     | 1.914s    | 4.50%       | 2.593s     | 36.776us      | 70500       |
| cudaStreamSynchronize                                    | 4.62%      | 2.508s    | 4.62%       | 2.508s    | 177.765us    | 88.182ms  | 0.21%       | 90.540ms   | 6.417us       | 14109       |
| Broadcast                                                | 2.87%      | 1.560s    | 5.44%       | 2.954s    | 419.067us    | 16.914s   | 39.80%      | 18.086s    | 2.565ms       | 7050        |
| cudaFree                                                 | 2.49%      | 1.351s    | 2.49%       | 1.352s    | 168.942ms    | 54.000us  | 0.00%       | 160.000us  | 20.000us      | 8           |
| cudaMemcpyAsync                                          | 2.01%      | 1.090s    | 2.01%       | 1.090s    | 25.768us     | 290.156ms | 0.68%       | 290.205ms  | 6.859us       | 42313       |
| enumerate(DataLoader)#_MultiProcessingDataLoaderIter...  | 1.96%      | 1.064s    | 1.98%       | 1.075s    | 152.417us    | 0.000us   | 0.00%       | 2.567ms    | 0.364us       | 7051        |
| Scatter                                                  | 1.78%      | 965.496ms | 8.22%       | 4.467s    | 316.816us    | 0.000us   | 0.00%       | 1.204s     | 85.395us      | 14100       |
| aten::cat                                                | 1.72%      | 933.110ms | 2.73%       | 1.483s    | 42.071us     | 1.122s    | 2.64%       | 1.434s     | 40.691us      | 35250       |
| aten::empty_strided                                      | 1.68%      | 914.378ms | 2.21%       | 1.200s    | 8.960us      | 0.000us   | 0.00%       | 2.492ms    | 0.019us       | 133958      |
| autograd::engine::evaluate_function: SigmoidBackward...  | 1.11%      | 604.317ms | 2.53%       | 1.373s    | 97.367us     | 0.000us   | 0.00%       | 138.496ms  | 9.822us       | 14100       |
| Gather                                                   | 1.06%      | 573.379ms | 3.08%       | 1.673s    | 237.243us    | 0.000us   | 0.00%       | 343.597ms  | 48.737us      | 7050        |
| aten::mean                                               | 0.82%      | 446.576ms | 1.09%       | 593.113ms | 42.041us     | 72.811ms  | 0.17%       | 199.439ms  | 14.137us      | 14108       |
| aten::div                                                | 0.78%      | 423.898ms | 1.20%       | 649.739ms | 46.081us     | 21.989ms  | 0.05%       | 86.182ms   | 6.112us       | 14100       |
| aten::view                                               | 0.66%      | 359.894ms | 0.66%       | 359.898ms | 1.823us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 197400      |
| Optimizer.zero_grad#Adam.zero_grad                       | 0.64%      | 345.174ms | 0.64%       | 345.287ms | 48.977us     | 0.000us   | 0.00%       | 2.365ms    | 0.335us       | 7050        |
| autograd::engine::evaluate_function: ViewBackward0       | 0.64%      | 349.936ms | 1.09%       | 594.948ms | 42.195us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| aten::to                                                 | 0.62%      | 339.339ms | 11.92%      | 6.478s    | 114.853us    | 0.000us   | 0.00%       | 2.159s     | 38.285us      | 56404       |
| aten::slice                                              | 0.57%      | 311.377ms | 0.65%       | 351.076ms | 4.980us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 70500       |
| aten::_to_copy                                           | 0.55%      | 300.738ms | 11.69%      | 6.351s    | 112.612us    | 0.000us   | 0.00%       | 2.244s     | 39.783us      | 56400       |
| cudaStreamWaitEvent                                      | 0.53%      | 285.441ms | 0.53%       | 285.441ms | 1.840us      | 1.066s    | 2.51%       | 1.066s     | 6.873us       | 155100      |
| GatherBackward                                           | 0.52%      | 280.917ms | 3.43%       | 1.866s    | 264.691us    | 0.000us   | 0.00%       | 322.285ms  | 45.714us      | 7050        |
| aten::empty                                              | 0.51%      | 277.471ms | 0.51%       | 277.711ms | 9.845us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28208       |
| cudaStreamIsCapturing                                    | 0.50%      | 270.481ms | 0.56%       | 302.159ms | 42.799us     | 75.014ms  | 0.18%       | 75.018ms   | 10.626us      | 7060        |
| aten::flatten_dense_tensors                              | 0.48%      | 258.388ms | 3.49%       | 1.894s    | 44.780us     | 0.000us   | 0.00%       | 1.434s     | 33.909us      | 42300       |
| aten::sub                                                | 0.41%      | 221.331ms | 0.60%       | 328.482ms | 46.593us     | 14.172ms  | 0.03%       | 46.475ms   | 6.592us       | 7050        |
| aten::narrow                                             | 0.38%      | 205.977ms | 1.01%       | 548.124ms | 7.775us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 70500       |
| aten::unflatten_dense_tensors                            | 0.37%      | 200.182ms | 1.41%       | 767.939ms | 27.232us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200       |
| aten::as_strided                                         | 0.36%      | 194.625ms | 0.36%       | 194.625ms | 0.863us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 225600      |
| aten::abs                                                | 0.34%      | 183.784ms | 1.06%       | 573.808ms | 40.696us     | 9.313ms   | 0.02%       | 77.692ms   | 5.510us       | 14100       |
| cudaOccupancyMaxActiveBlocksPerMultiprocessor            | 0.33%      | 180.372ms | 0.33%       | 180.755ms | 4.314us      | 380.393ms | 0.90%       | 380.427ms  | 9.079us       | 41904       |
| autograd::engine::evaluate_function: GatherBackward      | 0.33%      | 176.901ms | 3.76%       | 2.043s    | 289.784us    | 0.000us   | 0.00%       | 322.285ms  | 45.714us      | 7050        |
| autograd::engine::evaluate_function: MeanBackward0       | 0.31%      | 171.056ms | 1.94%       | 1.057s    | 74.949us     | 0.000us   | 0.00%       | 86.182ms   | 6.112us       | 14100       |
| aten::split_with_sizes                                   | 0.27%      | 145.889ms | 0.31%       | 169.224ms | 12.002us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| aten::sgn                                                | 0.24%      | 130.900ms | 0.39%       | 211.807ms | 30.044us     | 12.639ms  | 0.03%       | 53.366ms   | 7.570us       | 7050        |
| aten::mul                                                | 0.23%      | 125.318ms | 0.36%       | 194.212ms | 27.548us     | 14.113ms  | 0.03%       | 47.869ms   | 6.790us       | 7050        |
| aten::reshape                                            | 0.22%      | 117.614ms | 0.35%       | 187.603ms | 13.305us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| cudaPointerGetAttributes                                 | 0.21%      | 111.585ms | 0.21%       | 111.768ms | 7.927us      | 88.879ms  | 0.21%       | 88.887ms   | 6.304us       | 14100       |
| aten::expand                                             | 0.21%      | 111.504ms | 0.24%       | 132.667ms | 9.409us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| MeanBackward0                                            | 0.19%      | 103.323ms | 1.63%       | 884.485ms | 62.729us     | 0.000us   | 0.00%       | 86.099ms   | 6.106us       | 14100       |
| aten::fill_                                              | 0.16%      | 85.992ms  | 0.35%       | 188.952ms | 26.771us     | 7.171ms   | 0.02%       | 82.671ms   | 11.713us      | 7058        |
| aten::l1_loss                                            | 0.15%      | 83.180ms  | 1.98%       | 1.074s    | 152.272us    | 0.000us   | 0.00%       | 203.217ms  | 28.825us      | 7050        |
| aten::empty_like                                         | 0.13%      | 68.256ms  | 0.51%       | 278.729ms | 13.174us     | 0.000us   | 0.00%       | 2.488ms    | 0.118us       | 21158       |
| aten::_reshape_alias                                     | 0.13%      | 70.324ms  | 0.13%       | 70.324ms  | 4.988us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| aten::split                                              | 0.12%      | 63.087ms  | 0.29%       | 158.227ms | 22.444us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050        |
| autograd::engine::evaluate_function: AbsBackward0        | 0.12%      | 67.513ms  | 0.98%       | 534.811ms | 75.860us     | 0.000us   | 0.00%       | 101.235ms  | 14.360us      | 7050        |
| cudaHostAlloc                                            | 0.11%      | 61.430ms  | 0.15%       | 82.959ms  | 3.457ms      | 2.513ms   | 0.01%       | 2.533ms    | 105.542us     | 24          |
| cudaHostRegister                                         | 0.11%      | 60.898ms  | 0.11%       | 60.912ms  | 3.807ms      | 17.000us  | 0.00%       | 17.000us   | 1.062us       | 16          |
| ViewBackward0                                            | 0.11%      | 57.074ms  | 0.44%       | 239.703ms | 17.000us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 14100       |
| aten::item                                               | 0.11%      | 59.793ms  | 0.14%       | 75.773ms  | 1.343us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 56402       |
| AbsBackward0                                             | 0.11%      | 61.279ms  | 0.86%       | 467.298ms | 66.283us     | 0.000us   | 0.00%       | 101.235ms  | 14.360us      | 7050        |
| aten::resize_                                            | 0.09%      | 51.392ms  | 0.09%       | 51.399ms  | 7.287us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7054        |
| INVALID                                                  | 0.08%      | 44.000ms  | 0.08%       | 44.000ms  | 0.780us      | 400.311ms | 0.94%       | 400.311ms  | 7.098us       | 56400       |
| autograd::engine::evaluate_function: SubBackward0        | 0.08%      | 41.824ms  | 0.08%       | 43.399ms  | 6.156us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050        |
| aten::view_as                                            | 0.08%      | 44.991ms  | 0.11%       | 59.272ms  | 2.102us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 28200       |
| aten::ones_like                                          | 0.08%      | 45.627ms  | 0.62%       | 335.004ms | 47.518us     | 0.000us   | 0.00%       | 72.541ms   | 10.290us      | 7050        |
| aten::chunk                                              | 0.07%      | 38.604ms  | 0.35%       | 189.892ms | 26.935us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050        |
| cudaStreamDestroy                                        | 0.04%      | 21.665ms  | 0.04%       | 21.668ms  | 3.095ms      | 35.000us  | 0.00%       | 70.000us   | 10.000us      | 7           |
| aten::_local_scalar_dense                                | 0.03%      | 17.280ms  | 0.03%       | 17.280ms  | 0.306us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 56402       |
| aten::broadcast_tensors                                  | 0.02%      | 12.554ms  | 0.02%       | 12.554ms  | 1.781us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050        |
| aten::randperm                                           | 0.02%      | 13.576ms  | 0.05%       | 27.195ms  | 6.799ms      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 4           |
| cudaFuncGetAttributes                                    | 0.01%      | 7.121ms   | 0.01%       | 7.121ms   | 1.972us      | 61.084ms  | 0.14%       | 61.084ms   | 16.916us      | 3611        |
| cudaMalloc                                               | 0.01%      | 5.041ms   | 0.01%       | 5.809ms   | 170.853us    | 2.558ms   | 0.01%       | 2.680ms    | 78.824us      | 34          |
| ncclKernel_Broadcast_RING_LL_Sum_int8_t(ncclDevComm\*... | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 16.914s   | 39.80%      | 16.914s    | 599.799us     | 28200       |
| void at::native::(anonymous namespace)::CatArrayBatc...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 1.122s    | 2.64%       | 1.122s     | 31.842us      | 35250       |
| Memcpy HtoD (Pinned -> Device)                           | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 846.044ms | 1.99%       | 846.044ms  | 60.003us      | 14100       |
| volta_sgemm_128x32_sliced1x4_tn                          | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 607.017ms | 1.43%       | 607.017ms  | 43.051us      | 14100       |
| Memcpy DtoH (Device -> Device)                           | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 553.095ms | 1.30%       | 553.095ms  | 26.151us      | 21150       |
| Memcpy HtoD (Device -> Device)                           | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 507.629ms | 1.19%       | 507.629ms  | 24.001us      | 21150       |
| volta_sgemm_64x32_sliced1x4_tn                           | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 168.333ms | 0.40%       | 168.333ms  | 11.939us      | 14100       |
| void splitKreduce_kernel<32, 16, int, float, float, ...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 87.065ms  | 0.20%       | 87.065ms   | 3.087us       | 28200       |
| void at::native::reduce_kernel<512, 1, at::native::R...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 72.819ms  | 0.17%       | 72.819ms   | 5.164us       | 14100       |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 29.373ms  | 0.07%       | 29.373ms   | 2.083us       | 14100       |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 28.293ms  | 0.07%       | 28.293ms   | 2.007us       | 14100       |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 14.172ms  | 0.03%       | 14.172ms   | 2.010us       | 7050        |
| void at::native::elementwise_kernel<128, 2, at::nati...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 14.137ms  | 0.03%       | 14.137ms   | 2.005us       | 7050        |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 14.113ms  | 0.03%       | 14.113ms   | 2.002us       | 7050        |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 12.639ms  | 0.03%       | 12.639ms   | 1.793us       | 7050        |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 9.313ms   | 0.02%       | 9.313ms    | 1.321us       | 7050        |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 7.852ms   | 0.02%       | 7.852ms    | 1.114us       | 7050        |
| Memcpy DtoD (Device -> Device)                           | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 7.485ms   | 0.02%       | 7.485ms    | 1.062us       | 7050        |
| void at::native::vectorized_elementwise_kernel<4, at...  | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 7.171ms   | 0.02%       | 7.171ms    | 1.016us       | 7058        |
| cudaThreadExchangeStreamCaptureMode                      | 0.00%      | 68.000us  | 0.00%       | 68.000us  | 1.417us      | 2.502ms   | 0.01%       | 2.502ms    | 52.125us      | 48          |
| cudaDeviceGetAttribute                                   | 0.00%      | 7.000us   | 0.00%       | 7.000us   | 0.219us      | 337.000us | 0.00%       | 337.000us  | 10.531us      | 32          |
| Memset (Device)                                          | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 24.000us  | 0.00%       | 24.000us   | 2.000us       | 12          |
| cudaDeviceGetPCIBusId                                    | 0.00%      | 85.000us  | 0.00%       | 88.000us  | 1.796us      | 175.000us | 0.00%       | 200.000us  | 4.082us       | 49          |
| cudaGetDeviceCount                                       | 0.00%      | 3.000us   | 0.00%       | 3.000us   | 0.091us      | 157.000us | 0.00%       | 157.000us  | 4.758us       | 33          |
| cudaHostGetDevicePointer                                 | 0.00%      | 1.570ms   | 0.00%       | 1.570ms   | 112.143us    | 12.000us  | 0.00%       | 12.000us   | 0.857us       | 14          |
| cudaDeviceCanAccessPeer                                  | 0.00%      | 25.000us  | 0.00%       | 25.000us  | 1.389us      | 119.000us | 0.00%       | 121.000us  | 6.722us       | 18          |
| cudaMemsetAsync                                          | 0.00%      | 391.000us | 0.00%       | 441.000us | 36.750us     | 108.000us | 0.00%       | 108.000us  | 9.000us       | 12          |
| Memcpy HtoD (Pageable -> Device)                         | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 10.000us  | 0.00%       | 10.000us   | 0.714us       | 14          |
| aten::resolve_neg                                        | 0.00%      | 0.000us   | 0.00%       | 0.000us   | 0.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2           |
| cudaDriverGetVersion                                     | 0.00%      | 1.000us   | 0.00%       | 1.000us   | 0.333us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 3           |
| SubBackward0                                             | 0.00%      | 1.575ms   | 0.00%       | 1.575ms   | 0.223us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 7050        |
| aten::scalar_tensor                                      | 0.00%      | 12.000us  | 0.00%       | 12.000us  | 6.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2           |
| aten::resolve_conj                                       | 0.00%      | 2.000us   | 0.00%       | 2.000us   | 1.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2           |
| cudaGetSymbolAddress                                     | 0.00%      | 2.000us   | 0.00%       | 2.000us   | 1.000us      | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2           |
| cudaStreamCreateWithFlags                                | 0.00%      | 275.000us | 0.00%       | 279.000us | 25.364us     | 0.000us   | 0.00%       | 2.000us    | 0.182us       | 11          |
| aten::random_                                            | 0.00%      | 55.000us  | 0.00%       | 55.000us  | 27.500us     | 0.000us   | 0.00%       | 0.000us    | 0.000us       | 2           |